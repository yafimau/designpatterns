﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.State
{
    public interface IStateController<T>
    {
        void Init();
        void GoToState(T state);
        void Start();
        void Finish(T state);
        void Update();
        void Awake();
        void Pause();
        void Resume();
        bool AreStatesLoaded { get; }
        T CurrentStateKey { get; }
    }
}
