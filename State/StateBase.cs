﻿using Yafimau.DesignPatterns.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.State
{
    public abstract class StateBase : ObservableBase, IState
    {
        private readonly IStateContext _context;

        private bool _isStateLoaded;

        public StateBase(IStateContext context)
        {
            _context = context;
        }

        public virtual void Init()
        {

        }

        public virtual void Start()
        {

        }

        public virtual void Update()
        {

        }

        public virtual void Awake()
        {
        }

        public virtual void Pause()
        {

        }

        public virtual void Resume()
        {

        }

        public virtual void Finish()
        {

        }

        protected virtual IStateContext Context
        {
            get { return _context;  }
        }

        public abstract bool IsLoaded
        {
            get;
        }
    }
}
