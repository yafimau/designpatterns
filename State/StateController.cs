﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.State
{
    public class StateController<T> : IStateController<T> 
    {
        private Dictionary<T, IState> _states;
        private IStateBuilder<T> _stateBuilder;
        private IState _currentState;
        private T _currentStateKey;
        private bool _areStatesLoaded;

        public bool AreStatesLoaded
        {
            get
            {
                return _areStatesLoaded;
            }
        }

        public T CurrentStateKey
        {
            get
            {
                return _currentStateKey;
            }
        }

        public StateController(IStateBuilder<T> builder) {
            _stateBuilder = builder;
        }

        public void Init()
        {
            _states = _stateBuilder.Build();

            //foreach (var state in _states.Keys)
            //{
            //    _states[state].Init();
            //}
        }

        public void GoToState(T state)
        {
            _currentState = _states[state];
            _currentStateKey = state;
        }

        public void Start()
        {
            _currentState.Start();
        }

        public void Finish(T stateName)
        {
            var state = _states[stateName];

            if(state != null)
            {
                state.Finish();
            }
        }

        public void Update()
        {
            _currentState.Update();

            if(_areStatesLoaded)
            {
                return;
            }

            foreach(var state in _states.Keys)
            {
                if(!_states[state].IsLoaded)
                {
                    return;
                }
            }

            _areStatesLoaded = true;
        }

        public void Awake()
        {
            _currentState.Awake();
        }

        public void Pause()
        {
            _currentState.Pause();
        }

        public void Resume()
        {
            _currentState.Resume(); 
        }
    }
}
