﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.State
{
    public interface IState
    {
        void Start();
        void Finish();
        void Init();
        void Update();
        void Awake();
        void Pause();
        void Resume();
        bool IsLoaded { get; }
    }
}
