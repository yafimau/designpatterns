﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Observer
{
    public abstract class NotificationBase : INotification
    {
        private INotificationParams notificationParams;

        public NotificationBase()
        {
        }

        public INotificationParams NotificationParams {
            get { return this.notificationParams; }
            protected set { this.notificationParams = value; }
        }
    }
}
