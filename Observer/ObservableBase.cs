﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Observer
{
    public class ObservableBase : IObservable
    {
        private readonly IList<IObserver> observers = new List<IObserver>();

        public virtual void RegisterObserver(IObserver observer)
        {
            this.observers.Add(observer);
        }

        public virtual void NotifyAll(INotification notification)
        {
            foreach (var observer in this.observers)
            {
                observer.Notify(notification);
            }
        }
    }
}
