﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Observer
{
    public class NotificationClassResolver
    {
        private Dictionary<Type, INotificationHandler> _notificationHandlers = new Dictionary<Type, INotificationHandler>();

        public void RegisterHandler<T>(INotificationHandler handler) where T : class, INotification
        {
            _notificationHandlers.Add(typeof(T), handler);
        }

        public void Handle(INotification notification)
        {
            _notificationHandlers[notification.GetType()].Handle(notification.NotificationParams);
        }

    }
}
