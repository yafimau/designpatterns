﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Observer
{
    public class NotificationMethodResolver
    {
        public delegate void NotificationHandlerMethod(INotificationParams notificationParams);

        private Dictionary<Type, NotificationHandlerMethod> notificationHandlers = new Dictionary<Type, NotificationHandlerMethod>();

        public void RegisterHandler<T>(NotificationHandlerMethod handlerMethod) where T : class, INotification
        {
            notificationHandlers.Add(typeof(T), handlerMethod);
        }

        public void Handle(INotification notification)
        {
            notificationHandlers[notification.GetType()].Invoke(notification.NotificationParams);
        }
    }
}
