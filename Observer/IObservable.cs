﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Observer
{
    public interface IObservable
    {
        void RegisterObserver(IObserver observer);
        void NotifyAll(INotification notification);
    }
}
