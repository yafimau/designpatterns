﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ServiceLocator
{
    public interface IService
    {
        void Init();
    }
}
