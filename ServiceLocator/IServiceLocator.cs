﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ServiceLocator
{
    public interface IServiceLocator
    {
        void RegisterService<I, T>(T service) where T : class, I where I : IService;
        IService Resolve<I>() where I : IService;
    }
}
