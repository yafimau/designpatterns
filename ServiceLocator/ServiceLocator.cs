﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ServiceLocator
{
    public class ServiceLocator : IServiceLocator
    {
        private readonly Dictionary<Type, IService> services = new Dictionary<Type, IService>();

        public void RegisterService<I,T>(T service) where T : class, I where I : IService
        {
            this.services.Add(typeof(I), service);
        }

        public IService Resolve<I>() where I : IService
        {
            return services[typeof(I)];
        }
    }
}
