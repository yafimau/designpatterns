﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Builder
{
    public abstract class GenericBuilder<T> : IGenericBuilder<T>
    {
        public abstract T Build();
    }
}
