﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Builder
{
    public interface IGenericBuilder<T>
    {
        T Build();
    }
}
