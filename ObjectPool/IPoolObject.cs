﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ObjectPool
{
    public interface IPoolObject
    {
        bool IsActive { get; }

        void Activate();
        void Deactivate();
    }
}
