﻿using Yafimau.DesignPatterns.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ObjectPool
{
    public interface IGenericObjectPool<T> where T : IPoolObject
    {
        void Init(int size, IGenericBuilder<T> builder);
        T Allocate();
        IEnumerable<T> GetAllActive();
        void Dispose(T poolObject);
    }
}
