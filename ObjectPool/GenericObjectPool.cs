﻿using Yafimau.DesignPatterns.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.ObjectPool
{
    public class GenericObjectPool<T> : IGenericObjectPool<T> where T : IPoolObject
    {
        private int _size;
        private T[] _pool;

        public void Init(int size, IGenericBuilder<T> builder)
        {
            _size = size;
            _pool = new T[_size];

            for(int i = 0; i < _size; i++)
            {
                _pool[i] = builder.Build();
            }
        }

        public IEnumerable<T> GetAllActive()
        {
            return _pool.Where(x => x.IsActive);
        }

        public T Allocate()
        {
            for(int i = 0; i < _size; i++)
            {
                if(!_pool[i].IsActive)
                {
                    _pool[i].Activate();
                    return _pool[i];
                }
            }

            throw new Exception("No objects available in pool");
        }

        public void Dispose(T poolObject)
        {
            for (int i = 0; i < _size; i++)
            {
                if(_pool[i].Equals(poolObject))
                {
                    _pool[i].Deactivate();
                }
            }
        }
    }
}
