﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yafimau.DesignPatterns.Component
{
    public interface IComponent
    {
        void Update();
        void Awake();
        void Init();
        void SetActive(bool active);
        void SetProgress(float progress);
        bool IsInitialized { get; }
    }
}
